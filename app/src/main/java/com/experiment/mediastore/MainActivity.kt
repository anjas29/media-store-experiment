package com.experiment.mediastore

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import com.experiment.mediastore.databinding.ActivityMainBinding
import java.io.*


class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    private val TAG = "MediaStore"

    lateinit var fileList: List<Media>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // getData(applicationContext)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            permissionRequest(200)
        }

        // getAllMediaFilesOnDevice(applicationContext)
    }

    fun getList() {
        Log.d(TAG, "getList")
        fileList = MediaStoreUtils.getAllMediaFilesOnDevice(applicationContext)!!
        Log.d(TAG, "count: ${fileList.size}")

        deleteFiles(fileList)
    }

    //only work in api 30
    @SuppressLint("NewApi")
    fun deleteFiles(files: List<Media>) {

        val uris: MutableList<Uri> = mutableListOf()

        //delete first two files
        for (i in 0..1) {
            uris.add(files[i].uri)
            Log.d(TAG, "file to delete: ${files[i].uri}")
        }
        try{
            val pendingIntent = MediaStore.createDeleteRequest(applicationContext.contentResolver, uris)

            startIntentSenderForResult(pendingIntent.intentSender, 200, null, 0, 0, 0)
        }catch(e: Exception){
            e.printStackTrace()
        }
    }

    @RequiresApi(23)
    private fun permissionRequest(code: Int) {
        Log.d(TAG, "req permission")
        requestPermissions(
            arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE
            ), code
        )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.size > 0 && grantResults[0] != -1) {
            Log.d(TAG, "onRequestPermissionsResult")
            getList()
        }
    }
}