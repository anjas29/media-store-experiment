package com.experiment.mediastore

import android.net.Uri


/**
 * Created by anjasmoro adi on 22/05/2021.
 * Email: anjasmoro.adi29@gmail.com
 */
data class Media(
    val id: String,
    val path: String,
    val size: Long,
    val lastModified: Long,
    val uri: Uri
)