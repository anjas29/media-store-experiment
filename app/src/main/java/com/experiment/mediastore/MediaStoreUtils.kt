package com.experiment.mediastore

import android.content.ContentUris
import android.content.Context
import android.database.MergeCursor
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import java.io.File

object MediaStoreUtils {
    fun getAllMediaFilesOnDevice(context: Context): List<Media>? {
        val files: MutableList<Media> = ArrayList()
        val uriExternal = MediaStore.Images.Media.EXTERNAL_CONTENT_URI

        try {
            val columns = arrayOf(
                MediaStore.Images.Media._ID,
                MediaStore.Images.Media.SIZE,
                MediaStore.Images.Media.DATE_MODIFIED,
                MediaStore.Images.Media.DATA
            )
            val cursor = MergeCursor(
                arrayOf(
                    context.contentResolver.query(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        columns,
                        null,
                        null,
                        null
                    )
                    /*context.contentResolver.query(
                        MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                        columns,
                        null,
                        null,
                        null
                    ),
                    context.contentResolver.query(
                        MediaStore.Images.Media.INTERNAL_CONTENT_URI,
                        columns,
                        null,
                        null,
                        null
                    ),
                    context.contentResolver.query(
                        MediaStore.Video.Media.INTERNAL_CONTENT_URI,
                        columns,
                        null,
                        null,
                        null
                    )*/
                )
            )
            cursor.moveToFirst()
            files.clear()
            while (!cursor.isAfterLast) {
                var path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA))
                var size = cursor.getLong(cursor.getColumnIndex(MediaStore.Images.Media.SIZE))
                var lastModified =
                    cursor.getLong(cursor.getColumnIndex(MediaStore.Images.Media.DATE_MODIFIED))
                /*val lastPoint = path.lastIndexOf(".")
                path = path.substring(0, lastPoint) + path.substring(lastPoint).toLowerCase()*/

                var id = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media._ID))

                val uriImage = Uri.withAppendedPath(uriExternal, "" + id)
                // val uriImage= ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id.toLong());
                val media = Media(id, path, size, lastModified, uriImage)
                files.add(media)
                Log.d("MediaStore", media.toString())
                cursor.moveToNext()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return files
    }
}